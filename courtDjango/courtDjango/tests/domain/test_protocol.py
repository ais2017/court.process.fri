from court.domain.protocol import *
from court.domain.records import *


def test_protocol_init():
    protocol = Protocol()

    assert len(protocol.records) == 0


def test_protocol_add_record():
    protocol = Protocol()
    record1 = Interrogation(full_name='Full Name',
                            cause='Tsar velel')
    record2 = ExpertCall(full_name='Dexter Morgan',
                         specialty='Blood specialist')
    record3 = Petition(initiator='Lawyer',
                       subject='Besit prokuror very much')
    record4 = Debate()
    protocol.add_record(record1)
    protocol.add_record(record2)
    protocol.add_record(record3)
    protocol.add_record(record4)

    assert protocol.records[0] == record1
    assert protocol.records[1] == record2
    assert protocol.records[2] == record3
    assert protocol.records[3] == record4


