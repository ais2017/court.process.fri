import unittest

from court.domain.case import *
from court.domain.hearing import *
from court.domain.participants import *
from court.exceptions import *


def test_case_init():
    judge = Judge(889933)
    case = Case(judge=judge,
                identifier=777)

    assert case.judge == judge
    assert case.identifier == 777
    assert case.verdict is None
    assert case.state == 'Open'


def test_case_add_hearing():
    judge = Judge(223344)
    case = Case(judge=judge,
                identifier=555)

    hearing1 = Hearing('Open', Lawyer(345266), Prosecutor(342352))
    hearing2 = Hearing('Close', Lawyer(439029), Prosecutor(293805))

    case.add_hearing(hearing1)
    case.add_hearing(hearing2)

    assert case.get_hearing(0) == hearing1
    assert case.get_hearing(1) == hearing2


def test_case_get_hearing():
    judge = Judge(223344)
    case = Case(judge=judge,
                identifier=555)

    hearing1 = Hearing('Open', Lawyer(345266), Prosecutor(342352))
    hearing2 = Hearing('Close', Lawyer(439029), Prosecutor(293805))

    case.add_hearing(hearing1)
    case.add_hearing(hearing2)

    assert case.get_hearing(0) == hearing1
    assert case.get_hearing(1) == hearing2


def test_case_get_all_hearing():
    judge = Judge(223344)
    case = Case(judge=judge,
                identifier=555)

    hearing1 = Hearing('Open', Lawyer(345266), Prosecutor(342352))
    hearing2 = Hearing('Close', Lawyer(439029), Prosecutor(293805))

    case.add_hearing(hearing1)
    case.add_hearing(hearing2)

    hearings = case.get_all_hearings()

    assert hearings[0] == hearing1
    assert hearings[1] == hearing2


def test_case_close():
    judge = Judge(889933)
    case = Case(judge=judge,
                identifier=777)
    case.close('Rasstrel')

    assert case.state == 'Close'
    assert case.verdict == 'Rasstrel'

    # Double close
    with unittest.TestCase().assertRaises(CaseDoubleClose) as cm:
        case.close('Double rasstrel')

    assert cm.exception.message == 'Case is already closed.'
