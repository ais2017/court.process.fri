class Protocol:

    def __init__(self):
        self._records = []

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    @property
    def records(self):
        return self._records

    def add_record(self, record):
        self._records.append(record)
