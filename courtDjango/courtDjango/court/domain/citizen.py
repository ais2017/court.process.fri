class Citizen():

    def __init__(self, myname):
        self._myname = myname
        self._mycaseId=[]
        self._myhearingId = 0

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    @property
    def myname(self):
        return self._myname

    @property
    def mycaseId(self):
        return self._mycaseId

    def add_mycaseId(self, caseId):
        self._mycaseId.append(caseId)

    @property
    def myhearingId(self):
        return self._myhearingId

    @myhearingId.setter
    def myhearingId(self, hearingId):
        self._myhearingId = hearingId