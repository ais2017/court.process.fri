from django.shortcuts import render, redirect
from django.contrib import messages
# Create your views here.
from django.http import HttpResponse
from court.usecases.gateways import SQLiteCaseGateway
from court.usecases.usecases import *
from django.contrib.messages import get_messages
from court.domain.citizen import Citizen

flag = False
db = 0
hearingNum = 0
people = []
currentUser = 'unknown'
madeInter = False
listOfCases = [{'caseId': '111', 'judge': 'judge1', 'full_name': 'fio judge1'},
                {'caseId': '222', 'judge': 'judge2', 'full_name': 'fio judge2'},
                {'caseId': '333', 'judge': 'judge3', 'full_name': 'fio judge3'},
               ]
cases = {}

def hello_world(request):
    global flag
    global db
    if request.method == "POST":
        verdict = request.POST.get('verdict')
        return render(request, 'hello_world.html')
    else:
        if flag == False:
            db = SQLiteCaseGateway('blah', create_db=True)
            flag = True

        global listOfCases
        context = {"listOfCases": listOfCases}
        return render(request, 'hello_world.html', context)

def hearing_details(request):
    global listOfCases
    global cases
    global currentUser
    if request.method == "POST":
        caseId = request.POST.get('caseId')
        print("caseId = "+caseId)
    flag = False
    judge = 0
    for elem in listOfCases:
        if elem['caseId'] == caseId:
            judge = elem['judge']
            flag = True
            onecase = open_case(db, judge, caseId)
            if currentUser != 'unknown':
                if currentUser not in cases.keys():
                    tmplist = []
                    tmplist.append(onecase)
                    cases.update({currentUser: tmplist})
                else:
                    cases[currentUser].append(onecase)

    if flag == False:
        messages.add_message(request, messages.INFO, "There are no such cases")
        return redirect('/hello/')
    context = {"caseId": caseId}
    return render(request, 'hearing_details.html', context)

def process(request, caseId):
    print("in process")
    prosecutorId=0
    lawyerId=0
    context = {"caseId":caseId}
    global db
    global listOfCases
    global cases
    if request.method == "POST":
        if request.POST.get('verdict') is not None:
            print("listOfCases = " + str(listOfCases))
            verdict = request.POST.get('verdict')
            finish_hearing(db,caseId,hearingNum,verdict)

            for key in cases.keys():
                if caseId in cases[key]:
                    cases[key].remove(caseId)


            for elem in listOfCases:
                if elem["caseId"] == caseId:
                    listOfCases.remove(elem)
                    break
            print("listOfCases = "+str(listOfCases))
            return redirect('/hello/')

        # print("open = "+str(request.POST.get('open'))+" closed = "+str(request.POST.get('closed')))
        if lawyerId is None or prosecutorId is None:
            prosecutorId = request.POST.get('prosecutorId')
            lawyerId = request.POST.get('lawyerId')
            messages.add_message(request, messages.INFO, "There are no such cases")
            return redirect('/hello/')
        elif request.POST.get('verdict') is None:
            type = 0
            if str(request.POST.get('open')) == "Open":
                type = 1
            elif str(request.POST.get('closed')) == "Close":
                type = 2
            else:
                type = 1
            add_hearing_to_case(db,caseId,type ,lawyerId, prosecutorId)
            start_hearing(db, caseId, hearingNum)
    return render(request, 'process.html', context)


def call_expert_view(request, caseId):
    global db
    global listOfCases
    global hearingNum
    context = {"caseId": caseId}
    if request.method == "POST":
        fio = request.POST.get('fio')
        specialty = request.POST.get('speciality')
        materials = request.POST.get('materials')
        print("request.POST.get('materials') = "+request.POST.get('materials'))
        call_expert(db, caseId, hearingNum, fio, specialty, materials)

        messages.add_message(request, messages.INFO, caseId)##
        return redirect('/process/'+caseId+'/')
    return render(request, 'call_expert.html', context)

def petition(request, caseId):
    global db
    global listOfCases
    global hearingNum
    context = {"caseId": caseId}
    if request.method == "POST":
        initiator = request.POST.get('initiator')
        theme = request.POST.get('theme')
        submit_petition(db, caseId, hearingNum, initiator, theme)
        messages.add_message(request, messages.INFO, caseId)  ##
        return redirect('/process/' + caseId + '/')
    return render(request, 'petition.html', context)


def interrogation(request, caseId):
    global db
    global listOfCases
    global hearingNum
    global madeInter
    context = {"caseId": caseId}
    if request.method == "POST" and request.POST.get('fio') is not None:
        fio = request.POST.get('fio')
        cause = request.POST.get('cause')
        rec_num = interrogate_witness(db, caseId, hearingNum, fio, cause)
        madeInter = True
        messages.add_message(request, messages.INFO, rec_num)
        return render(request, 'interrogation.html', context)
    elif request.method == "POST" and request.POST.get('add_question') is not None and madeInter == True:
        question = request.POST.get('add_question')
        mess = get_messages(request)
        recNum = 0
        for el in mess:
            # if el.level == 3:
            print("el = " + str(el.message))
            recNum = int(el.message)
        print("recNum = " + str(recNum))
        add_question(db, caseId, hearingNum, 0 , question)
        return render(request, 'interrogation.html', context)
    elif request.method == "POST" and request.POST.get('add_answer') is not None and madeInter == True:
        answer = request.POST.get('add_answer')
        mess = get_messages(request)
        recNum = 0
        for el in mess:
            # if el.level == 3:
            print("el = " + str(el.message))
            recNum = int(el.message)
        print("recNum = "+str(recNum))
        add_answer(db, caseId, hearingNum, 0, answer)
        return render(request, 'interrogation.html', context)
    elif request.method == "POST":
        return render(request, 'interrogation.html', context)
    return redirect('/process/'+caseId+'/')


def start_page (request):
    return render(request, "startPage.html")


def citizen (request):
    global listOfCases
    global people
    global currentUser
    if request.method == "POST" and request.POST.get('cname') is not None:
        name = request.POST.get('cname')
        currentUser = name
        man = Citizen(name)
        global flag
        # global db
        # if flag == False:
        #     db = SQLiteCaseGateway('blah', create_db=True)
        #     flag = True
        # db.addCititzen(man)
        people.append(man)
        flag = len(cases)
        if name in cases.keys():
            context = {"listOfCases": listOfCases, "man": name, "cases": cases[name], "flag": flag}
        else:
            context = {"listOfCases": listOfCases, "man": name, "cases": [], "flag": flag}
        return render(request, "citizen.html", context)
    elif currentUser != 'unknown':
        if currentUser in cases.keys():
            context = {"listOfCases": listOfCases, "man": currentUser, "cases": cases[currentUser], "flag": flag}
        else:
            context = {"listOfCases": listOfCases, "man": currentUser, "cases": [], "flag": flag}
        return render(request, "citizen.html", context)
    return render(request, "citizen.html")


def citizen_chose(request, man):
    if request.method == "POST":
        global listOfCases
        global people
        caseId = request.POST.get('caseId')
        for elem in people:
            if elem.myname == man:
                if caseId not in elem.mycaseId:
                    elem.mycaseId.append(caseId)

        return redirect('/citizen/')
    else:
        return redirect('/hello/')

