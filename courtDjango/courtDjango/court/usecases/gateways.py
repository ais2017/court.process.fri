from abc import ABC, abstractmethod
import ast
from datetime import datetime
import copy
import os
from pathlib import Path

import sqlite3

from court.domain.case import Case
from court.domain.participants import Judge, Prosecutor, Lawyer
from court.domain.hearing import Hearing
from court.domain.records import *
from court.domain.speech import *

####################################
import pymongo
from pymongo import MongoClient
import time


class AbstractCaseGateway(ABC):
    def __init__(self):
        pass

    # Return None if case doesn't exist
    # Return Case object if it exists
    @abstractmethod
    def get_by_id(self, case_id):
        pass

    # If filter is omitted method returns all hearings of all case
    # If filter is 'Active' method returns all active (not closed) hearings
    # If filter is 'Today' method returns all hearings that was started today and not closed yet
    # If filter is something else method raises AttributeError exception
    @abstractmethod
    def get_all_hearings(self, filter_key='All'):
        pass

    @abstractmethod
    def update(self, case):
        pass

    @abstractmethod
    def create(self, case):
        pass


class TestCaseGateway(AbstractCaseGateway):

    def get_by_id(self, case_id):
        # Stub
        if case_id == 555555:
            case = Case(Judge(442233), 555555)
            hearing1 = Hearing(h_type='Close',
                               lawyer=Lawyer(228833),
                               prosecutor=Prosecutor(223311))
            interrogation = Interrogation('Yura Molotok', 'Tsar velel')
            expert_call = ExpertCall('Dexter Morgan', 'Blood specialist')
            hearing1.protocol.add_record(interrogation)  # 0
            hearing1.protocol.add_record(expert_call)    # 1
            hearing1.start_hearing()
            case.add_hearing(hearing1)
            hearing2 = Hearing(h_type='Open',
                               lawyer=Lawyer(885533),
                               prosecutor=Prosecutor(773311))
            case.add_hearing(hearing2)
            return case
        else:
            return None

    def get_all_hearings(self, filter_key='All'):
        cases = TestCaseGateway._get_all()
        hearings = []

        if filter_key == 'All':
            for case in cases:
                hearings.extend(case.get_all_hearings())
            return hearings
        elif filter_key == 'Active':
            for case in cases:
                if case.state == 'Open':
                    hearings.extend(case.get_all_hearings())
            return hearings
        elif filter_key == 'Today':
            for case in cases:
                hearings.extend(case.get_all_hearings())

            today_hearings = []
            for hearing in hearings:
                if hearing.start_time.day == datetime.now().day:
                    today_hearings.append(hearing)
            return today_hearings
        else:
            raise AttributeError("Filter key is not valid.")

    def update(self, case):
        # convert case to something that we can store and send it to storage
        pass

    def create(self, case):
        # create and save new case
        pass

    # For internal testing purposes
    @staticmethod
    def _get_all():
        # Stub
        case1 = Case(Judge(442233), 111111)
        hearing1 = Hearing(h_type='Close',
                           lawyer=Lawyer(228833),
                           prosecutor=Prosecutor(223311))
        hearing1.start_hearing()
        case1.add_hearing(hearing1)
        hearing2 = Hearing(h_type='Open',
                           lawyer=Lawyer(885533),
                           prosecutor=Prosecutor(773311))
        hearing2.start_hearing()
        case1.add_hearing(hearing2)
        case2 = copy.deepcopy(case1)
        case2._identifier = 222222
        case2.close('Rasstrel')
        case3 = copy.deepcopy(case1)
        case3._identifier = 333333
        return [case1, case2, case3]


class SQLiteCaseGateway(AbstractCaseGateway):
    def __init__(self, db_path, create_db=False):
        super(SQLiteCaseGateway, self).__init__()
        self._db_path = db_path

        # If you want create database file and add all necessary tables
        if create_db is True:
             self._create_db()

    # def addCititzen(self, man):
    #     name = self.citizens.find_one({'myname': man.myname})
    #     print("name is none? name = "+name)
    #     if name is None:
    #         man = {'myname': man.myname,
    #                     'mycaseId': man.mycaseId,
    #                     'myhearingId': man.myhearingId,
    #                     }
    #         self.citizens.insert_one(man)

    # Raise CaseNotExists exception if case doesn't exist
    # Return Case object if it exists
    def get_by_id(self, case_id):
        # If case doesn't exist exception will raise
        # If case exists we get information for case building
        result = self.court_case.find_one({'caseId': case_id})
        if result is None:
            raise CaseAlreadyExists("Case with case ID {0} already exists.".format(case_id))

        # Create Judge for case
        judge = Judge(identifier=result['judgeId'])

        # Create case
        case = Case(judge, identifier=case_id, verdict=result['verdict'], state=result['state'])

        # Get all hearings that belong to the case
        result = self.hearing.find({'caseId': case_id})
        hearings = self._create_hearings_from_rows(result)
        for hearing in hearings:
            case.add_hearing(hearing)

        # Get all interrogation records that belong to the case
        result = self.interrogation_record.find({'caseId': case_id})
        for row in result:
            interrogation = Interrogation(full_name=row['full_name'],
                                          cause=row['cause'],
                                          readings=row['readings'],
                                          fix_flag=True if row['fix_flag'] == 'True' else False)
            case.get_hearing(row['hearing_number']).protocol.add_record(interrogation)

        # Get all expert call records that belong to the case
        result = self.expert_call_record.find({'caseId': case_id})
        for row in result:
            expert_call = ExpertCall(full_name=row['full_name'],
                                     specialty=row['specialty'],
                                     materials=ast.literal_eval(row['materials']),  # Create list from str
                                     fix_flag=True if row['fix_flag'] == 'True' else False)
            case.get_hearing(row['hearing_number']).protocol.add_record(expert_call)


        # Get all petition records that belong to the case
        result = self.petition_record.find({'caseId': case_id})
        for row in result:
            petition = Petition(initiator=row['initiator'],
                                subject=row['subject'],
                                decision=row['decision'],
                                fix_flag=True if row['fix_flag'] == 'True' else False)
            case.get_hearing(row['hearing_number']).protocol.add_record(petition)

        return case

    # If filter is omitted method returns all hearings of all case
    # If filter is 'Active' method returns all active (not closed) hearings
    # If filter is 'Today' method returns all hearings that was started today and not closed yet
    # If filter is something else method raises AttributeError exception
    def get_all_hearings(self, filter_key='All'):
        if filter_key not in ['All', 'Active', 'Today']:
            raise AttributeError("Filter key is not valid.")

        hearings = []

        if filter_key == 'All':
            tmp = self.hearing.find()
            hearings = self._create_hearings_from_rows(tmp)
        elif filter_key == 'Active':
            whereStateOpen = self.court_case.find({'state':'Open'})
            result = []
            for tmp in whereStateOpen:
                doc = self.hearing.find_one({'caseId': tmp['caseId']})
                result.append(doc)

            hearings = self._create_hearings_from_rows(result)
        elif filter_key == 'Today':
            all = self.hearing.find()
            result = []
            for tmp in all:
                if time.strftime('%d',tmp['start_datetime'])== time.strftime('%d', datetime.now()):
                    result.append(tmp)
            hearings = self._create_hearings_from_rows(result)

        return hearings

    def create(self, case):
        print("in create")
        # If case exists exception will raise
        result = self.court_case.find_one({'caseId': case.identifier})
        if result is not None:
            raise CaseAlreadyExists("Case with case ID {0} already exists.".format(case.identifier))

        # Insert case
        thiscase = {'caseId': case.identifier,
                'judgeId': case.judge.identifier,
                'verdict': str(case.verdict),
                'state': case.state
                }
        self.court_case.insert_one(thiscase)

        # Insert hearings
        print("case.get_all_hearings() yo = " + str(case.get_all_hearings()))
        for hearing_number, hearing in enumerate(case.get_all_hearings()):
            hearing = {'caseId': case.identifier,
                     'h_number': hearing_number,
                     'h_type': hearing.h_type,
                     'h_verdict': hearing.verdict,
                     'start_time': str(hearing.start_time),
                     'finish_time': str(hearing.finish_time),
                     'lawyerId': hearing.lawyer.identity,
                     'prosecutorId': hearing.prosecutor.identity}
            print("inserted one before")
            self.hearing.insert_one(hearing)
            print("inserted one after")

        # Insert records
        for hearing_number, hearing in enumerate(case.get_all_hearings()):
            for record_number, record in enumerate(hearing.protocol.records):
                element = {}
                print("record.rcd_type = "+str(record.rcd_type))
                if record.rcd_type == 'Interrogation':
                    element = {'caseId': case.identifier,
                              'hearing_number': hearing_number,
                              'record_number': record_number,
                              'type': record.rcd_type,
                              'fix_flag': str(record.fix_flag),
                              'full_name': record.full_name,
                              'cause': record.cause,
                              'readings': record.readings}
                    self.interrogation_record.insert_one(element)
                elif record.rcd_type == 'Expert call':
                    element = {'caseId': case.identifier,
                              'hearing_number': hearing_number,
                              'record_number': record_number,
                              'type': record.rcd_type,
                              'fix_flag': str(record.fix_flag),
                              'full_name': record.full_name,
                              'specialty': record.specialty,
                              'materials': str(record.materials)}
                    self.expert_call_record.insert_one(element)
                elif record.rcd_type == 'Petition':
                    element = {'caseId': case.identifier,
                              'hearing_number': hearing_number,
                              'record_number': record_number,
                              'type': record.rcd_type,
                              'fix_flag': str(record.fix_flag),
                              'initiator': record.initiator,
                              'subject': record.subject,
                              'decision': record.decision}
                    self.petition_record.insert_one(element)


    def update(self, case):
        # If case doesn't exist exception will raise
        result = self.court_case.find_one({'caseId': case.identifier})
        if result is None:
            raise CaseAlreadyExists("Case with case ID {0} already exists.".format(case.identifier))

        # Delete existing case and its hearings with all records
        self.court_case.delete_many({'caseId': case.identifier})
        self.hearing.delete_many({'caseId': case.identifier})
        self.interrogation_record.delete_many({'caseId': case.identifier})
        self.expert_call_record.delete_many({'caseId': case.identifier})
        self.petition_record.delete_many({'caseId': case.identifier})

        # Create updated case
        self.create(case)

    # Create Hearing objects from database rows
    @staticmethod
    def _create_hearings_from_rows(rows):
        hearings = []
        for row in rows:
            hearing = Hearing(h_type=row['h_type'],
                              verdict=row['h_verdict'],
                              start_time=None if row['start_time'] == 'None' else datetime.strptime(str(row['start_time']), "%Y-%m-%d %H:%M:%S.%f"),
                              finish_time=None if row['finish_time'] == 'None' else datetime.strptime(str(row['finish_time']), "%Y-%m-%d %H:%M:%S.%f"),
                              lawyer=Lawyer(row['lawyerId']),
                              prosecutor=Prosecutor(row['prosecutorId'])
                              )
            hearings.append(hearing)
        return hearings


    def _create_db(self):
        print("create_db!!!!!!!!!")
        client = MongoClient('mongodb://localhost:27017/')

        # Delete database if exists
        dbnames = client.database_names()
        if 'mydb' in dbnames:
            print("create_db droped!!!!!!!!!")
            client.drop_database('mydb')

        self.my_mongo_db = client.mydb
        self.court_case = self.my_mongo_db.court_case
        self.hearing = self.my_mongo_db.hearing
        self.interrogation_record = self.my_mongo_db.interrogation_record
        self.expert_call_record = self.my_mongo_db.expert_call_record
        self.petition_record = self.my_mongo_db.petition_record
        self.participant = self.my_mongo_db.participant
        self.citizens = self.my_mongo_db.citizens

        # sql_command = """
        # CREATE TABLE court_case (
        #     case_id INTEGER PRIMARY KEY,
        #     judge_id INTEGER,
        #     verdict TEXT,
        #     state TEXT);"""
        # cursor.execute(sql_command)
        #
        # sql_command = """
        # CREATE TABLE hearing (
        #     case_id INTEGER,
        #     hearing_number INTEGER,
        #     type TEXT,
        #     verdict INTEGER,
        #     start_datetime TEXT,
        #     finish_datetime TEXT,
        #     lawyer_id INTEGER,
        #     prosecutor_id INTEGER,
        #     PRIMARY KEY (case_id, hearing_number));"""
        # cursor.execute(sql_command)
        #
        # sql_command = """
        # CREATE TABLE interrogation_record (
        #     case_id INTEGER,
        #     hearing_number INTEGER,
        #     record_number INTEGER,
        #     type TEXT,
        #     fix_flag TEXT,
        #     full_name TEXT,
        #     cause TEXT,
        #     readings TEXT,
        #     PRIMARY KEY (case_id, hearing_number, record_number));"""
        # cursor.execute(sql_command)
        #
        # sql_command = """
        # CREATE TABLE expert_call_record (
        #     case_id INTEGER,
        #     hearing_number INTEGER,
        #     record_number INTEGER,
        #     type TEXT,
        #     fix_flag TEXT,
        #     full_name TEXT,
        #     specialty TEXT,
        #     materials TEXT,
        #     PRIMARY KEY (case_id, hearing_number, record_number));"""
        # cursor.execute(sql_command)
        #
        # sql_command = """
        # CREATE TABLE petition_record (
        #     case_id INTEGER,
        #     hearing_number INTEGER,
        #     record_number INTEGER,
        #     type TEXT,
        #     fix_flag TEXT,
        #     initiator TEXT,
        #     subject TEXT,
        #     decision TEXT,
        #     PRIMARY KEY (case_id, hearing_number, record_number));"""
        # cursor.execute(sql_command)
        #
        # sql_command = """
        # CREATE TABLE participant (
        #     participant_id INTEGER PRIMARY KEY,
        #     role TEXT,
        #     payload TEXT);"""
        # cursor.execute(sql_command)
        #
        # connection.commit()
        # connection.close()


