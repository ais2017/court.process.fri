from django.conf.urls import url
from court.views import hello_world, hearing_details, interrogation, petition, call_expert_view, process, start_page, citizen, citizen_chose


urlpatterns = [
    url(r'^$', start_page),
    url(r'^citizen/$', citizen),
    url(r'^citizen/(?P<man>\w+)/$', citizen_chose),
    url(r'^hello/$', hello_world),
    url(r'^hearing/$', hearing_details),
    url(r'^process/(?P<caseId>\w+)/$', process),
    url(r'^call_expert/(?P<caseId>\w+)/$', call_expert_view),
    url(r'^petition/(?P<caseId>\w+)/$', petition),
    url(r'^interrogation/(?P<caseId>\w+)/*$', interrogation),
]