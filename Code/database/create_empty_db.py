#!/usr/bin/python
import os
from pathlib import Path
import sqlite3

# Delete database if exists
database = Path("/media/data/studies/ais/Code/database/database.db")
if database.exists():
	os.remove("/media/data/studies/ais/Code/database/database.db")

connection = sqlite3.connect("/media/data/studies/ais/Code/database/database.db")

cursor = connection.cursor()

# ---------------------------------- #
# ------------ Creating ------------ #
# ---------------------------------- #

sql_command = """
CREATE TABLE court_case ( 
	case_id INTEGER PRIMARY KEY,
	judge_id INTEGER,
	verdict TEXT,
	state TEXT);"""
cursor.execute(sql_command)

sql_command = """
CREATE TABLE hearing ( 
	case_id INTEGER,
	hearing_number INTEGER,
	type TEXT,
	verdict INTEGER,
	start_datetime TEXT,
	finish_datetime TEXT,
	lawyer_id INTEGER,
	prosecutor_id INTEGER,
	PRIMARY KEY (case_id, hearing_number));"""
cursor.execute(sql_command)

sql_command = """
CREATE TABLE interrogation_record (
	case_id INTEGER, 
	hearing_number INTEGER,
	record_number INTEGER,
	type TEXT,
	fix_flag TEXT,
	full_name TEXT,
	cause TEXT,
	readings TEXT,
	PRIMARY KEY (case_id, hearing_number, record_number));"""
cursor.execute(sql_command)

sql_command = """
CREATE TABLE expert_call_record (
	case_id INTEGER, 
	hearing_number INTEGER,
	record_number INTEGER,
	type TEXT,
	fix_flag TEXT,
	full_name TEXT,
	specialty TEXT,
	materials TEXT,
	PRIMARY KEY (case_id, hearing_number, record_number));"""
cursor.execute(sql_command)

sql_command = """
CREATE TABLE petition_record (
	case_id INTEGER, 
	hearing_number INTEGER,
	record_number INTEGER,
	type TEXT,
	fix_flag TEXT,
	initiator TEXT,
	subject TEXT,
	decision TEXT,
	PRIMARY KEY (case_id, hearing_number, record_number));"""
cursor.execute(sql_command)

sql_command = """
CREATE TABLE participant ( 
	participant_id INTEGER PRIMARY KEY,
	role TEXT,
	payload TEXT);"""
cursor.execute(sql_command)

# ---------------------------------- #
# ------------ Inserts ------------- #
# ---------------------------------- #

sql_command = """
	INSERT INTO participant (participant_id, role, payload)
    VALUES (0, "Judge", "{identifier: 111}");"""
cursor.execute(sql_command)
sql_command = """
	INSERT INTO participant (participant_id, role, payload)
    VALUES (1, "Lawyer", "{identity: 222}");"""
cursor.execute(sql_command)
sql_command = """
	INSERT INTO participant (participant_id, role, payload)
    VALUES (2, "Prosecutor", "{identity: 333}");"""
cursor.execute(sql_command)

connection.commit()
connection.close()
