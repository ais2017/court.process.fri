import os
from pathlib import Path
import unittest

from court.usecases.gateways import *
from court.usecases.usecases import *


def test_get_hearings_by_case_id():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    hearings_tuples = get_hearings_by_case_id(gateway, 555555)
    assert hearings_tuples[0][0] == 'Close'
    assert hearings_tuples[0][1] == 228833
    assert hearings_tuples[0][2] == 223311

    assert hearings_tuples[1][0] == 'Open'
    assert hearings_tuples[1][1] == 885533
    assert hearings_tuples[1][2] == 773311


def test_get_all_hearings():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    hearings_tuples = get_all_hearings(gateway)
    assert len(hearings_tuples) == 8
    for i in range(3):
        assert hearings_tuples[i*2][0] == 'Close'
        assert hearings_tuples[i*2 + 1][0] == 'Open'


def test_get_all_active_hearings():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    hearings_tuples = get_all_active_hearings(gateway)
    assert len(hearings_tuples) == 6


def test_get_all_today_hearings():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    hearings_tuples = get_all_today_hearings(gateway)
    assert len(hearings_tuples) == 7
    for hearing_tuple in hearings_tuples:
        assert hearing_tuple[3].day == datetime.now().day


def test_open_case():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    open_case(gateway, 665577, 777777)
    case = gateway.get_by_id(777777)

    assert case.identifier == 777777
    assert case.judge.identifier == 665577

    with unittest.TestCase().assertRaises(CaseAlreadyExists) as cm:
        open_case(gateway, 665577, 777777)
    assert cm.exception.message == "Case with case ID 777777 already exists."


# def test_close_case():
#     refresh_db()
#     gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
#     close_case(gateway, 555555, 'Next hearing')
#     case = gateway.get_by_id(555555)
#
#     assert case.identifier == 555555
#     assert case.state == 'Close'
#     assert case.verdict == 'Next hearing'
#
#     with unittest.TestCase().assertRaises(CaseNotExists) as cm:
#         close_case(gateway, 777777, 'Next hearing')
#     assert cm.exception.message == "Case with case ID 777777 doesn't exist."


def test_add_hearing_to_case():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    add_hearing_to_case(gateway, 555555, 'Open', 334455, 338822)
    with unittest.TestCase().assertRaises(CaseNotExists) as cm:
        add_hearing_to_case(gateway, 777777, 'Open', 334455, 338822)
    assert cm.exception.message == "Case with case ID 777777 doesn't exist."


def test_start_hearing():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    start_hearing(gateway, 555555, 1)

    with unittest.TestCase().assertRaises(HearingNotExists) as cm:
        start_hearing(gateway, 555555, 9)
    assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"


def test_finish_hearing():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    finish_hearing(gateway, 555555, 0, 'Rasstrel')

    with unittest.TestCase().assertRaises(HearingNotExists) as cm:
        finish_hearing(gateway, 555555, 9, 'Rasstrel')
    assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"


def test_call_expert():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    call_expert(gateway, 555555, 0, 'Dexter Morgan', 'Blood specialist', ['Topor', 'Nagan'])

    with unittest.TestCase().assertRaises(HearingNotExists) as cm:
        call_expert(gateway, 555555, 9, 'Dexter Morgan', 'Blood specialist', ['Topor', 'Nagan'])
    assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"

    with unittest.TestCase().assertRaises(MaterialDuplication) as cm:
        call_expert(gateway, 555555, 0, 'Dexter Morgan', 'Blood specialist', ['Topor', 'Topor'])
    assert cm.exception.message == 'Material "Topor" is already added to material.'


def test_interrogate_witness():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    interrogate_witness(gateway, 555555, 0, 'Krolik Banni', 'Tsar velel')

    with unittest.TestCase().assertRaises(HearingNotExists) as cm:
        interrogate_witness(gateway, 555555, 9, 'Krolik Banni', 'Tsar velel')
    assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"


def test_add_question():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    add_question(gateway, 555555, 0, 0, 'Ti kto takoi?')
    case = gateway.get_by_id(555555)

    assert case.get_all_hearings()[0].protocol.records[0].readings == 'Question: Ti kto takoi?\n'

    with unittest.TestCase().assertRaises(HearingNotExists) as cm:
        add_question(gateway, 555555, 9, 0, 'Ti kto takoi?')
    assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"


def test_add_answer():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    add_answer(gateway, 555555, 0, 0, 'I am the end and the beginning.')
    case = gateway.get_by_id(555555)

    assert case.get_all_hearings()[0].protocol.records[0].readings == 'Answer: I am the end and the beginning.\n'

    with unittest.TestCase().assertRaises(HearingNotExists) as cm:
        add_question(gateway, 555555, 9, 0, 'I am the end and the beginning.')
    assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"


# def test_hold_debate():
#     refresh_db()
#     result = hold_debate(gateway, 555555, 0)
#     assert result[0] is True
#
#     with unittest.TestCase().assertRaises(HearingNotExists) as cm:
#         hold_debate(gateway, 555555, 9)
#     assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"
#
#
# def test_add_victim_debate_speech():
#     refresh_db()
#     result = add_victim_debate_speech(gateway, 555555, 0, 2, '22 44 102934', "Speech")
#     assert result[0] is True
#
#     result = add_victim_debate_speech(gateway, 555555, 0, 1, '22 44 102934', "Speech")
#     assert result[0] is False
#
#     with unittest.TestCase().assertRaises(HearingNotExists) as cm:
#         add_victim_debate_speech(gateway, 555555, 9, 0, '22 44 102934', "Speech")
#     assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"
#
#
# def test_add_prosecutor_debate_speech():
#     refresh_db()
#     result = add_prosecutor_debate_speech(gateway, 555555, 0, 2, 889966, "Speech")
#     assert result[0] is True
#
#     result = add_prosecutor_debate_speech(gateway, 555555, 0, 1, 996644, "Speech")
#     assert result[0] is False
#
#     with unittest.TestCase().assertRaises(HearingNotExists) as cm:
#         add_prosecutor_debate_speech(gateway, 555555, 9, 0, 996644, "Speech")
#     assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"
#
#
# def test_add_lawyer_debate_speech():
#     refresh_db()
#     result = add_lawyer_debate_speech(gateway, 555555, 0, 3, 775522, "Speech")
#     assert result[0] is True
#
#     result = add_lawyer_debate_speech(gateway, 555555, 0, 0, 996622, "Speech")
#     assert result[0] is False
#
#     with unittest.TestCase().assertRaises(HearingNotExists) as cm:
#         add_lawyer_debate_speech(gateway, 555555, 9, 3, 996655, "Speech")
#     assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"
#
#
# def test_add_accused_debate_speech():
#     refresh_db()
#     result = add_accused_debate_speech(gateway, 555555, 0, 4, '22 44 102934', "Speech")
#     assert result[0] is True
#
#     result = add_accused_debate_speech(gateway, 555555, 0, 0, '22 44 102934', "Speech")
#     assert result[0] is False
#
#     with unittest.TestCase().assertRaises(HearingNotExists) as cm:
#         add_accused_debate_speech(gateway, 555555, 9, 4, '22 44 102934', "Speech")
#     assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"


def test_submit_petition():
    refresh_db()
    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db')
    submit_petition(gateway, 555555, 0, 'Aist', 'Murder')
    case = gateway.get_by_id(555555)

    assert case.get_all_hearings()[0].protocol.records[2].initiator == 'Aist'
    assert case.get_all_hearings()[0].protocol.records[2].subject == 'Murder'

    with unittest.TestCase().assertRaises(HearingNotExists) as cm:
        submit_petition(gateway, 555555, 9, 'Aist', 'Murder')
    assert cm.exception.message == "Hearing with number 9 doesn't exist in case with id 555555"


# Refresh database for correct testing
def refresh_db():
    # Delete database if exists
    database = Path("/media/data/studies/ais/Code/database/database_for_tests.db")
    if database.exists():
        os.remove("/media/data/studies/ais/Code/database/database_for_tests.db")

    case0 = Case(Judge(442233), 555555)
    hearing1 = Hearing(h_type='Close',
                       lawyer=Lawyer(228833),
                       prosecutor=Prosecutor(223311))
    interrogation = Interrogation('Yura Molotok', 'Tsar velel')
    expert_call = ExpertCall('Dexter Morgan', 'Blood specialist')
    hearing1.protocol.add_record(interrogation)  # 0
    hearing1.protocol.add_record(expert_call)  # 1
    hearing1.start_hearing()
    case0.add_hearing(hearing1)
    hearing2 = Hearing(h_type='Open',
                       lawyer=Lawyer(885533),
                       prosecutor=Prosecutor(773311))
    case0.add_hearing(hearing2)

    case1 = Case(Judge(442233), 111111)
    hearing1 = Hearing(h_type='Close',
                       lawyer=Lawyer(228833),
                       prosecutor=Prosecutor(223311))
    hearing1.start_hearing()
    case1.add_hearing(hearing1)
    hearing2 = Hearing(h_type='Open',
                       lawyer=Lawyer(885533),
                       prosecutor=Prosecutor(773311))
    hearing2.start_hearing()
    case1.add_hearing(hearing2)
    case2 = copy.deepcopy(case1)
    case2._identifier = 222222
    case2.close('Rasstrel')
    case3 = copy.deepcopy(case1)
    case3._identifier = 333333

    gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database_for_tests.db', True)

    gateway.create(case0)
    gateway.create(case1)
    gateway.create(case2)
    gateway.create(case3)