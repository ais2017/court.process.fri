from court.domain.participants import *


def test_secretary_init():
    participant = Secretary(identifier=7)

    assert participant.role == 'Secretary'
    assert participant.identifier == 7


def test_judge_init():
    participant = Judge(identifier=9)

    assert participant.role == 'Judge'
    assert participant.identifier == 9


def test_prosecutor_init():
    participant = Prosecutor(identity=424242)

    assert participant.role == 'Prosecutor'
    assert participant.identity == 424242


def test_investigator_init():
    participant = Investigator(rank='80 lvl',
                               passport='78 56 412567',
                               identity=771177)

    assert participant.role == 'Investigator'
    assert participant.rank == '80 lvl'
    assert participant.passport == '78 56 412567'
    assert participant.identity == 771177


def test_interrogator_init():
    participant = Interrogator(rank='80 lvl',
                               passport='78 52 412563',
                               identity=772277)

    assert participant.role == 'Interrogator'
    assert participant.rank == '80 lvl'
    assert participant.passport == '78 52 412563'
    assert participant.identity == 772277


def test_accused_init():
    participant = Accused(passport='32 12 098789')

    assert participant.role == 'Accused'
    assert participant.passport == '32 12 098789'


def test_lawyer_init():
    participant = Lawyer(identity=552277)

    assert participant.role == 'Lawyer'
    assert participant.identity == 552277


def test_victim_init():
    participant = Victim(passport='23 42 126423')

    assert participant.role == 'Victim'
    assert participant.passport == '23 42 126423'
