import unittest

from court.domain.hearing import *
from court.domain.participants import *
from court.exceptions import *


def test_hearing_init():
    lawyer = Lawyer(700012)
    prosecutor = Prosecutor(100053)
    hearing = Hearing(h_type='Open',
                      lawyer=lawyer,
                      prosecutor=prosecutor)

    assert hearing.h_type == 'Open'
    assert hearing.lawyer == lawyer
    assert hearing.prosecutor == prosecutor
    assert hearing.verdict == 'Not passed'
    assert hearing.start_time is None
    assert hearing.finish_time is None


def test_hearing_start_hearing():
    lawyer = Lawyer(700012)
    prosecutor = Prosecutor(100053)
    hearing = Hearing(h_type='Open',
                      lawyer=lawyer,
                      prosecutor=prosecutor)
    time_before = datetime.now()
    hearing.start_hearing()
    time_after = datetime.now()

    assert hearing.start_time > time_before
    assert hearing.start_time < time_after

    # Double start
    with unittest.TestCase().assertRaises(HearingAlreadyStarted) as cm:
        hearing.start_hearing()

    assert cm.exception.message == 'Hearing is already started.'


def test_hearing_finish_hearing():
    lawyer = Lawyer(700012)
    prosecutor = Prosecutor(100053)
    hearing = Hearing(h_type='Open',
                      lawyer=lawyer,
                      prosecutor=prosecutor)
    time_before = datetime.now()
    hearing.finish_hearing('Pass sentence')
    time_after = datetime.now()

    assert hearing.finish_time > time_before
    assert hearing.finish_time < time_after
    assert hearing.verdict == 'Pass sentence'

    # Double finish

    with unittest.TestCase().assertRaises(HearingAlreadyFinished) as cm:
        hearing.finish_hearing('Not passed')

    assert cm.exception.message == 'Hearing is already finished.'

