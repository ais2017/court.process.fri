import unittest

from court.domain.records import *
from court.domain.participants import *
from court.domain.speech import *


def test_interrogation_init():
    record = Interrogation(full_name='Full Name',
                           cause='Tsar velel')

    assert record.rcd_type == 'Interrogation'
    assert record.fix_flag is False
    assert record.full_name == 'Full Name'
    assert record.cause == 'Tsar velel'

    record.fix_record()

    assert record.fix_flag is True


def test_interrogation_readings():
    record = Interrogation(full_name='Full Name',
                           cause='Tsar velel')
    record.add_question('Why are you grohnul old Alena Ivanovna?')
    record.add_answer('I wanted to know whether I am a trembling creature or '
                      'whether I have the right.')
    record.add_question('Did you get the answer?')
    record.add_answer('Only suffering and nothing more.')

    assert record.readings == "Question: Why are you grohnul old Alena Ivanovna?\n" \
                              "Answer: I wanted to know whether " \
                              "I am a trembling creature or whether I have the right.\n" \
                              "Question: Did you get the answer?\n" \
                              "Answer: Only suffering and nothing more.\n"


def test_expert_call_init():
    record = ExpertCall(full_name='Dexter Morgan',
                        specialty='Blood specialist')

    assert record.rcd_type == 'Expert call'
    assert record.fix_flag is False
    assert record.full_name == 'Dexter Morgan'
    assert record.specialty == 'Blood specialist'
    assert len(record.materials) == 0


def test_expert_call_add_material():
    record = ExpertCall(full_name='Dexter Morgan',
                        specialty='Blood specialist')
    record.add_material('About topor')
    record.add_material('About hammer')

    assert record.materials == ['About topor', 'About hammer']

    # Add existing material

    with unittest.TestCase().assertRaises(MaterialDuplication) as cm:
        record.add_material('About topor')

    assert cm.exception.message == 'Material "About topor" is already added to material.'


def test_petition_init():
    record = Petition(initiator='Lawyer',
                      subject='Besit prokuror very much')

    assert record.rcd_type == 'Petition'
    assert record.fix_flag is False
    assert record.initiator == 'Lawyer'
    assert record.subject == 'Besit prokuror very much'
    assert record.decision is None


def test_petition_approve_petition():
    record = Petition(initiator='Lawyer',
                      subject='Besit prokuror very much')
    record.approve_petition()

    assert record.decision == 'Approved'


def test_petition_dismiss_petition():
    record = Petition(initiator='Lawyer',
                      subject='Besit prokuror very much')
    record.dismiss_petition()

    assert record.decision == 'Dismissed'


def test_debate_init():
    record = Debate()

    assert record.rcd_type == 'Debate'
    assert record.fix_flag is False
    assert len(record.speeches) == 0


def test_debate_add_speech():
    victim_speech = Speech(Victim('90 32 123512'), 'This stupid sobaka broke my nail!')
    prosecutor_speech = Speech(Prosecutor(443355), "His place in tur'ma! Two or three pozgiznenih!")
    lawyer_speech = Speech(Lawyer(334422), 'But it was only a toenail!')
    accused_speech = Speech(Accused('23 72 632323'), "Mom was right. I shouldn't have become a pedicure master.")

    # Wait successful pass
    record = Debate()

    record.add_speech(victim_speech)
    record.add_speech(prosecutor_speech)
    record.add_speech(lawyer_speech)
    record.add_speech(accused_speech)

    assert record.speeches[0] == victim_speech
    assert record.speeches[1] == prosecutor_speech
    assert record.speeches[2] == lawyer_speech
    assert record.speeches[3] == accused_speech

    # Wait successful pass
    record = Debate()

    record.add_speech(prosecutor_speech)
    record.add_speech(lawyer_speech)
    record.add_speech(accused_speech)

    assert record.speeches[0] == prosecutor_speech
    assert record.speeches[1] == lawyer_speech
    assert record.speeches[2] == accused_speech

    # Wait exceptions about first speech
    record = Debate()
    with unittest.TestCase().assertRaises(DebateOrderBroken) as cm:
        record.add_speech(lawyer_speech)

    assert cm.exception.message == 'Only victim or prosecutor can be first.'

    # Wait exceptions about speech order
    record = Debate()
    record.add_speech(victim_speech)
    with unittest.TestCase().assertRaises(DebateOrderBroken) as cm:
        record.add_speech(lawyer_speech)

    assert cm.exception.message == 'Debate order is broken.'

