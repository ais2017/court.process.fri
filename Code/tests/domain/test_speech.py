from court.domain.speech import *
from court.domain.participants import *


def test_speech_init():
    participant = Accused('12 80 431056')
    speech = Speech(participant=participant,
                    speech_text='I am Andrey and I have a cow. I love cows.')

    assert speech.participant == participant
    assert speech.speech_text == 'I am Andrey and I have a cow. I love cows.'

