import copy

from court.domain.case import *
from court.domain.participants import *
from court.domain.hearing import *
from court.domain.records import *

from court.usecases.gateways import SQLiteCaseGateway


def main():
    # case = Case(Judge(111), 555555)
    # hearing1 = Hearing(h_type='Close',
    #                    lawyer=Lawyer(222),
    #                    prosecutor=Prosecutor(333))
    # interrogation = Interrogation('Yura Molotok', 'Tsar velel')
    # interrogation.add_question('Who are you?')
    # interrogation.add_answer('I am Alpha and Omega. The beginning and the end.')
    # interrogation.fix_record()
    # expert_call = ExpertCall('Dexter Morgan', 'Blood specialist')
    # expert_call.add_material('Topor')
    # expert_call.fix_record()
    # hearing1.start_hearing()
    # hearing1.protocol.add_record(interrogation)  # 0
    # hearing1.protocol.add_record(expert_call)
    # case.add_hearing(hearing1)
    # hearing2 = Hearing(h_type='Open',
    #                    lawyer=Lawyer(222),
    #                    prosecutor=Prosecutor(333))
    # petition = Petition('Prosecutor', 'Lawyer besit very much')
    # petition.approve_petition()
    # petition.fix_record()
    # hearing2.start_hearing()
    # hearing2.protocol.add_record(petition)
    # case.add_hearing(hearing2)

    # case_changed = copy.deepcopy(case)
    # hearing3 = Hearing(h_type='Open',
    #                    lawyer=Lawyer(222),
    #                    prosecutor=Prosecutor(333))
    # petition = Petition('Prosecutor', 'Lawyer besit very much')
    # petition.approve_petition()
    # petition.fix_record()
    # hearing3.start_hearing()
    # hearing3.protocol.add_record(petition)
    # case_changed.add_hearing(hearing3)

    case = Case(Judge(442233), 555555)
    hearing1 = Hearing(h_type='Close',
                       lawyer=Lawyer(228833),
                       prosecutor=Prosecutor(223311))
    interrogation = Interrogation('Yura Molotok', 'Tsar velel')
    expert_call = ExpertCall('Dexter Morgan', 'Blood specialist')
    hearing1.protocol.add_record(interrogation)  # 0
    hearing1.protocol.add_record(expert_call)  # 1
    hearing1.start_hearing()
    case.add_hearing(hearing1)
    hearing2 = Hearing(h_type='Open',
                       lawyer=Lawyer(885533),
                       prosecutor=Prosecutor(773311))
    case.add_hearing(hearing2)

    case1 = Case(Judge(442233), 111111)
    hearing1 = Hearing(h_type='Close',
                       lawyer=Lawyer(228833),
                       prosecutor=Prosecutor(223311))
    hearing1.start_hearing()
    case1.add_hearing(hearing1)
    hearing2 = Hearing(h_type='Open',
                       lawyer=Lawyer(885533),
                       prosecutor=Prosecutor(773311))
    hearing2.start_hearing()
    case1.add_hearing(hearing2)
    case2 = copy.deepcopy(case1)
    case2._identifier = 222222
    case2.close('Rasstrel')
    case3 = copy.deepcopy(case1)
    case3._identifier = 333333

    gateway = SQLiteCaseGateway('database.db', True)
    gateway.create(case)
    gateway.create(case1)
    gateway.create(case2)
    gateway.create(case3)
    # gateway.create(case)
    # case_copy = gateway.get_by_id(555555)
    # case_copy._identifier = 777777
    # case_copy._state = 'Close'
    # gateway.create(case_copy)
    # hearings = gateway.get_all_hearings(filter_key='Today')
    # for hearing in hearings:
    #     print(hearing.h_type)
    #     print(hearing.lawyer.identity)
    #     print(hearing.prosecutor.identity)
    #     print(hearing.verdict)
    #     print(hearing.start_time)
    #     print(hearing.finish_time)
    # gateway.update(case_changed)


if __name__ == '__main__':
    main()
