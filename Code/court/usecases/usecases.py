from court.domain.participants import *
from court.usecases.gateways import *


# ------------------------------- #
# ------- Local functions ------- #
# ------------------------------- #

def hearings_to_tuples(hearings):
    hearing_tuples = []
    for hearing in hearings:
        hearing_tuples.append((hearing.h_type,
                               hearing.lawyer.identity,
                               hearing.prosecutor.identity,
                               hearing.start_time,
                               hearing.finish_time))
    return hearing_tuples


# ------------------------------- #
# ---------- Citizen ------------ #
# ------------------------------- #


def get_hearings_by_case_id(gateway, case_id):
    case = gateway.get_by_id(case_id)
    return hearings_to_tuples(case.get_all_hearings())


# All hearings of all cases
def get_all_hearings(gateway):
    all_hearings = gateway.get_all_hearings('All')
    return hearings_to_tuples(all_hearings)


# All hearings of all open cases
def get_all_active_hearings(gateway):
    all_hearings = gateway.get_all_hearings('Active')
    return hearings_to_tuples(all_hearings)


# --------------------------------- #
# ---------- Secretary ------------ #
# --------------------------------- #


def get_all_today_hearings(gateway):
    all_hearings = gateway.get_all_hearings('Today')
    return hearings_to_tuples(all_hearings)


# Create new case and register in the system
def open_case(gateway, judge_id, case_id):
    judge = Judge(judge_id)
    case = Case(judge, case_id)
    gateway.create(case)


def close_case(gateway, case_id, verdict):
    case = gateway.get_by_id(case_id)
    case.close(verdict)
    gateway.update(case)


def add_hearing_to_case(gateway, case_id, hearing_type,
                        lawyer_id, prosecutor_id):
    # Get existing case
    case = gateway.get_by_id(case_id)

    # Create internal entities
    lawyer = Lawyer(lawyer_id)
    prosecutor = Prosecutor(prosecutor_id)
    hearing = Hearing(hearing_type, lawyer, prosecutor)
    case.add_hearing(hearing)

    # Save changed case
    gateway.update(case)


def start_hearing(gateway, case_id, hearing_number):
    case = gateway.get_by_id(case_id)
    hearing = case.get_hearing(hearing_number)
    hearing.start_hearing()
    gateway.update(case)


def finish_hearing(gateway, case_id, hearing_number, verdict):
    case = gateway.get_by_id(case_id)
    hearing = case.get_hearing(hearing_number)
    hearing.finish_hearing(verdict)
    gateway.update(case)


def call_expert(gateway, case_id, hearing_number, full_name, specialty, materials):
    case = gateway.get_by_id(case_id)
    hearing = case.get_hearing(hearing_number)
    record = ExpertCall(full_name, specialty)
    # record._materials.clear()
    for material in materials:
        record.add_material(material)
    hearing.protocol.add_record(record)
    gateway.update(case)


def interrogate_witness(gateway, case_id, hearing_number, full_name, cause):
    case = gateway.get_by_id(case_id)
    hearing = case.get_hearing(hearing_number)
    record = Interrogation(full_name, cause)
    record_number = len(hearing.protocol.records)
    hearing.protocol.add_record(record)
    gateway.update(case)
    return record_number


def add_question(gateway, case_id, hearing_number, record_number, question):
    case = gateway.get_by_id(case_id)
    hearing = case.get_hearing(hearing_number)
    record = hearing.protocol.records[record_number]
    if record.rcd_type != 'Interrogation':
        raise UseCaseError("Record type is not 'Interrogation'")
    else:
        record.add_question(question)
        gateway.update(case)


def add_answer(gateway, case_id, hearing_number, record_number, answer):
    case = gateway.get_by_id(case_id)
    hearing = case.get_hearing(hearing_number)
    record = hearing.protocol.records[record_number]
    if record.rcd_type != 'Interrogation':
        raise UseCaseError("Record type is not 'Interrogation'")
    else:
        record.add_answer(answer)
        gateway.update(case)


# def hold_debate(gateway, case_id, hearing_number):
#     case = gateway.get_by_id(case_id)
#     hearing = case.get_hearing(hearing_number)
#     record = Debate()
#     hearing.protocol.add_record(record)
#     gateway.update(case)
#     return True, "Debate record was successfully added."
#
#
# def add_victim_debate_speech(gateway, case_id, hearing_number, record_number, passport, speech_text):
#     case = gateway.get_by_id(case_id)
#     hearing = case.get_hearing(hearing_number)
#     record = hearing.protocol.records[record_number]
#     if record.rcd_type != 'Debate':
#         return False, "Record type is not 'Debate'"
#     else:
#         victim = Victim(passport)
#         speech = Speech(victim, speech_text)
#         record.add_speech(speech)
#         gateway.update(case)
#         return True, "Victim's debate speech was successfully added."
#
#
# def add_prosecutor_debate_speech(gateway, case_id, hearing_number, record_number, identity, speech_text):
#     case = gateway.get_by_id(case_id)
#     hearing = case.get_hearing(hearing_number)
#     record = hearing.protocol.records[record_number]
#     if record.rcd_type != 'Debate':
#         return False, "Record type is not 'Debate'"
#     else:
#         prosecutor = Prosecutor(identity)
#         speech = Speech(prosecutor, speech_text)
#         record.add_speech(speech)
#         gateway.update(case)
#         return True, "Prosecutor's debate speech was successfully added."
#
#
# def add_lawyer_debate_speech(gateway, case_id, hearing_number, record_number, identity, speech_text):
#     case = gateway.get_by_id(case_id)
#     hearing = case.get_hearing(hearing_number)
#     record = hearing.protocol.records[record_number]
#     if record.rcd_type != 'Debate':
#         return False, "Record type is not 'Debate'"
#     else:
#         lawyer = Lawyer(identity)
#         speech = Speech(lawyer, speech_text)
#         record.add_speech(speech)
#         gateway.update(case)
#         return True, "Lawyer's debate speech was successfully added."
#
#
# def add_accused_debate_speech(gateway, case_id, hearing_number, record_number, passport, speech_text):
#     case = gateway.get_by_id(case_id)
#     hearing = case.get_hearing(hearing_number)
#     record = hearing.protocol.records[record_number]
#     if record.rcd_type != 'Debate':
#         return False, "Record type is not 'Debate'"
#     else:
#         accused = Accused(passport)
#         speech = Speech(accused, speech_text)
#         record.add_speech(speech)
#         gateway.update(case)
#         return True, "Accused's debate speech was successfully added."


def submit_petition(gateway, case_id, hearing_number, initiator, subject):
    case = gateway.get_by_id(case_id)
    hearing = case.get_hearing(hearing_number)
    record = Petition(initiator, subject)
    hearing.protocol.add_record(record)
    gateway.update(case)
