from abc import ABC, abstractmethod
import ast
from datetime import datetime
import copy
import os
from pathlib import Path

import sqlite3

from court.domain.case import Case
from court.domain.participants import Judge, Prosecutor, Lawyer
from court.domain.hearing import Hearing
from court.domain.records import *
from court.domain.speech import *


class AbstractCaseGateway(ABC):
    def __init__(self):
        pass

    # Return None if case doesn't exist
    # Return Case object if it exists
    @abstractmethod
    def get_by_id(self, case_id):
        pass

    # If filter is omitted method returns all hearings of all case
    # If filter is 'Active' method returns all active (not closed) hearings
    # If filter is 'Today' method returns all hearings that was started today and not closed yet
    # If filter is something else method raises AttributeError exception
    @abstractmethod
    def get_all_hearings(self, filter_key='All'):
        pass

    @abstractmethod
    def update(self, case):
        pass

    @abstractmethod
    def create(self, case):
        pass


class TestCaseGateway(AbstractCaseGateway):

    def get_by_id(self, case_id):
        # Stub
        if case_id == 555555:
            case = Case(Judge(442233), 555555)
            hearing1 = Hearing(h_type='Close',
                               lawyer=Lawyer(228833),
                               prosecutor=Prosecutor(223311))
            interrogation = Interrogation('Yura Molotok', 'Tsar velel')
            expert_call = ExpertCall('Dexter Morgan', 'Blood specialist')
            hearing1.protocol.add_record(interrogation)  # 0
            hearing1.protocol.add_record(expert_call)    # 1
            hearing1.start_hearing()
            case.add_hearing(hearing1)
            hearing2 = Hearing(h_type='Open',
                               lawyer=Lawyer(885533),
                               prosecutor=Prosecutor(773311))
            case.add_hearing(hearing2)
            return case
        else:
            return None

    def get_all_hearings(self, filter_key='All'):
        cases = TestCaseGateway._get_all()
        hearings = []

        if filter_key == 'All':
            for case in cases:
                hearings.extend(case.get_all_hearings())
            return hearings
        elif filter_key == 'Active':
            for case in cases:
                if case.state == 'Open':
                    hearings.extend(case.get_all_hearings())
            return hearings
        elif filter_key == 'Today':
            for case in cases:
                hearings.extend(case.get_all_hearings())

            today_hearings = []
            for hearing in hearings:
                if hearing.start_time.day == datetime.now().day:
                    today_hearings.append(hearing)
            return today_hearings
        else:
            raise AttributeError("Filter key is not valid.")

    def update(self, case):
        # convert case to something that we can store and send it to storage
        pass

    def create(self, case):
        # create and save new case
        pass

    # For internal testing purposes
    @staticmethod
    def _get_all():
        # Stub
        case1 = Case(Judge(442233), 111111)
        hearing1 = Hearing(h_type='Close',
                           lawyer=Lawyer(228833),
                           prosecutor=Prosecutor(223311))
        hearing1.start_hearing()
        case1.add_hearing(hearing1)
        hearing2 = Hearing(h_type='Open',
                           lawyer=Lawyer(885533),
                           prosecutor=Prosecutor(773311))
        hearing2.start_hearing()
        case1.add_hearing(hearing2)
        case2 = copy.deepcopy(case1)
        case2._identifier = 222222
        case2.close('Rasstrel')
        case3 = copy.deepcopy(case1)
        case3._identifier = 333333
        return [case1, case2, case3]


class SQLiteCaseGateway(AbstractCaseGateway):
    def __init__(self, db_path, create_db=False):
        super(SQLiteCaseGateway, self).__init__()
        self._db_path = db_path

        # If you want create database file and add all necessary tables
        if create_db is True:
            self._create_db()

    # Raise CaseNotExists exception if case doesn't exist
    # Return Case object if it exists
    def get_by_id(self, case_id):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        # If case doesn't exist exception will raise
        # If case exists we get information for case building
        cur.execute("SELECT judge_id, verdict, state FROM court_case WHERE case_id = :case_id;", {'case_id': case_id})
        result = cur.fetchone()
        if result is None:
            raise CaseNotExists("Case with case ID {0} doesn't exist.".format(case_id))

        # Create Judge for case
        judge = Judge(identifier=result[0])

        # Create case
        case = Case(judge, identifier=case_id, verdict=result[1], state=result[2])

        # Get all hearings that belong to the case
        cur.execute("SELECT * FROM hearing WHERE case_id = :case_id;", {'case_id': case_id})
        hearings = self._create_hearings_from_rows(cur.fetchall())
        for hearing in hearings:
            case.add_hearing(hearing)

        # Get all interrogation records that belong to the case
        cur.execute("""
                    SELECT hearing_number,fix_flag,full_name,cause,readings
                    FROM interrogation_record WHERE case_id = :case_id;
                    """, {'case_id': case_id})
        for row in cur.fetchall():
            interrogation = Interrogation(full_name=row[2],
                                          cause=row[3],
                                          readings=row[4],
                                          fix_flag=True if row[1] == 'True' else False)
            case.get_hearing(row[0]).protocol.add_record(interrogation)

        # Get all expert call records that belong to the case
        cur.execute("""
                    SELECT hearing_number,fix_flag,full_name,specialty,materials
                    FROM expert_call_record WHERE case_id = :case_id;
                    """, {'case_id': case_id})
        for row in cur.fetchall():
            expert_call = ExpertCall(full_name=row[2],
                                     specialty=row[3],
                                     materials=ast.literal_eval(row[4]),  # Create list from str
                                     fix_flag=True if row[1] == 'True' else False)
            case.get_hearing(row[0]).protocol.add_record(expert_call)

        # Get all petition records that belong to the case
        cur.execute("""
                    SELECT hearing_number,fix_flag,initiator,subject,decision
                    FROM petition_record WHERE case_id = :case_id;
                    """, {'case_id': case_id})
        for row in cur.fetchall():
            petition = Petition(initiator=row[2],
                                subject=row[3],
                                decision=row[4],
                                fix_flag=True if row[1] == 'True' else False)
            case.get_hearing(row[0]).protocol.add_record(petition)

        connection.commit()
        connection.close()

        return case

    # If filter is omitted method returns all hearings of all case
    # If filter is 'Active' method returns all active (not closed) hearings
    # If filter is 'Today' method returns all hearings that was started today and not closed yet
    # If filter is something else method raises AttributeError exception
    def get_all_hearings(self, filter_key='All'):
        if filter_key not in ['All', 'Active', 'Today']:
            raise AttributeError("Filter key is not valid.")

        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()
        hearings = []

        if filter_key == 'All':
            cur.execute("SELECT * FROM hearing;")
            hearings = self._create_hearings_from_rows(cur.fetchall())
        elif filter_key == 'Active':
            cur.execute("""
                        SELECT *
                        FROM hearing WHERE case_id IN (SELECT case_id FROM court_case WHERE state = "Open");
                        """)
            hearings = self._create_hearings_from_rows(cur.fetchall())
        elif filter_key == 'Today':
            cur.execute("""
                        SELECT *
                        FROM hearing WHERE strftime('%d', start_datetime) = strftime('%d', 'now');
                        """)
            hearings = self._create_hearings_from_rows(cur.fetchall())

        connection.close()
        return hearings

    def create(self, case):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        # If case exists exception will raise
        cur.execute("SELECT case_id FROM court_case WHERE case_id = :case_id;", {'case_id': case.identifier})
        result = cur.fetchone()
        if result is not None:
            raise CaseAlreadyExists("Case with case ID {0} already exists.".format(case.identifier))

        # Insert case
        sql_command = """
                    INSERT INTO court_case (case_id, judge_id, verdict, state)
                    VALUES (:case_id, :judge_id, :verdict, :state);"""
        cur.execute(sql_command, {'case_id': case.identifier,
                                  'judge_id': case.judge.identifier,
                                  'verdict': str(case.verdict),
                                  'state': case.state})

        # Insert hearings
        for hearing_number, hearing in enumerate(case.get_all_hearings()):
            sql_command = """
                INSERT INTO hearing (case_id,hearing_number,type,verdict,start_datetime,finish_datetime,lawyer_id,prosecutor_id)
                VALUES (:case_id, :h_number, :h_type, :h_verdict, :start_time, :finish_time, :lawyer_id, :prosecutor_id);"""
            cur.execute(sql_command, {'case_id': case.identifier,
                                      'h_number': hearing_number,
                                      'h_type': hearing.h_type,
                                      'h_verdict': hearing.verdict,
                                      'start_time': str(hearing.start_time),
                                      'finish_time': str(hearing.finish_time),
                                      'lawyer_id': hearing.lawyer.identity,
                                      'prosecutor_id': hearing.prosecutor.identity})

        # Insert records
        for hearing_number, hearing in enumerate(case.get_all_hearings()):
            for record_number, record in enumerate(hearing.protocol.records):
                if record.rcd_type == 'Interrogation':
                    sql_command = """
                        INSERT INTO interrogation_record (case_id,hearing_number,record_number,type,fix_flag,full_name,cause,readings)
                        VALUES (:case_id,:hearing_number,:record_number,:type,:fix_flag,:full_name,:cause,:readings);"""
                    cur.execute(sql_command, {'case_id': case.identifier,
                                              'hearing_number': hearing_number,
                                              'record_number': record_number,
                                              'type': record.rcd_type,
                                              'fix_flag': str(record.fix_flag),
                                              'full_name': record.full_name,
                                              'cause': record.cause,
                                              'readings': record.readings})
                elif record.rcd_type == 'Expert call':
                    sql_command = """
                        INSERT INTO expert_call_record (case_id,hearing_number,record_number,type,fix_flag,full_name,specialty,materials)
                        VALUES (:case_id,:hearing_number,:record_number,:type,:fix_flag,:full_name,:specialty,:materials);"""
                    cur.execute(sql_command, {'case_id': case.identifier,
                                              'hearing_number': hearing_number,
                                              'record_number': record_number,
                                              'type': record.rcd_type,
                                              'fix_flag': str(record.fix_flag),
                                              'full_name': record.full_name,
                                              'specialty': record.specialty,
                                              'materials': str(record.materials)})
                elif record.rcd_type == 'Petition':
                    sql_command = """
                        INSERT INTO petition_record (case_id,hearing_number,record_number,type,fix_flag,initiator,subject,decision)
                        VALUES (:case_id,:hearing_number,:record_number,:type,:fix_flag,:initiator,:subject,:decision);""" \
                        .format(case.identifier, hearing_number, record_number, record.rcd_type, str(record.fix_flag),
                                record.initiator, record.subject, record.decision)
                    cur.execute(sql_command, {'case_id': case.identifier,
                                              'hearing_number': hearing_number,
                                              'record_number': record_number,
                                              'type': record.rcd_type,
                                              'fix_flag': str(record.fix_flag),
                                              'initiator': record.initiator,
                                              'subject': record.subject,
                                              'decision': record.decision})

        connection.commit()
        connection.close()

    def update(self, case):
        connection = sqlite3.connect(self._db_path)
        cur = connection.cursor()

        # If case doesn't exist exception will raise
        cur.execute("SELECT case_id FROM court_case WHERE case_id = :case_id;", {'case_id': case.identifier})
        result = cur.fetchone()
        if result is None:
            raise CaseNotExists("Case with case ID {0} doesn't exist.".format(case.identifier))

        # Delete existing case and its hearings with all records
        cur.execute("DELETE FROM court_case WHERE case_id = :case_id;", {'case_id': case.identifier})
        cur.execute("DELETE FROM hearing WHERE case_id = :case_id;", {'case_id': case.identifier})
        cur.execute("DELETE FROM interrogation_record WHERE case_id = :case_id;", {'case_id': case.identifier})
        cur.execute("DELETE FROM expert_call_record WHERE case_id = :case_id;", {'case_id': case.identifier})
        cur.execute("DELETE FROM petition_record WHERE case_id = :case_id;", {'case_id': case.identifier})

        connection.commit()
        connection.close()

        # Create updated case
        self.create(case)

    # Create Hearing objects from database rows
    @staticmethod
    def _create_hearings_from_rows(rows):
        hearings = []
        for row in rows:
            hearing = Hearing(h_type=row[2],
                              verdict=row[3],
                              start_time=None if row[4] == 'None' else datetime.strptime(str(row[4]), "%Y-%m-%d %H:%M:%S.%f"),
                              finish_time=None if row[5] == 'None' else datetime.strptime(str(row[5]), "%Y-%m-%d %H:%M:%S.%f"),
                              lawyer=Lawyer(row[6]),
                              prosecutor=Prosecutor(row[7])
                              )
            hearings.append(hearing)
        return hearings

    def _create_db(self):
        # Delete database if exists
        database = Path(self._db_path)
        if database.exists():
            os.remove(self._db_path)

        connection = sqlite3.connect(self._db_path)
        cursor = connection.cursor()

        sql_command = """
        CREATE TABLE court_case (
            case_id INTEGER PRIMARY KEY,
            judge_id INTEGER,
            verdict TEXT,
            state TEXT);"""
        cursor.execute(sql_command)

        sql_command = """
        CREATE TABLE hearing (
            case_id INTEGER,
            hearing_number INTEGER,
            type TEXT,
            verdict INTEGER,
            start_datetime TEXT,
            finish_datetime TEXT,
            lawyer_id INTEGER,
            prosecutor_id INTEGER,
            PRIMARY KEY (case_id, hearing_number));"""
        cursor.execute(sql_command)

        sql_command = """
        CREATE TABLE interrogation_record (
            case_id INTEGER,
            hearing_number INTEGER,
            record_number INTEGER,
            type TEXT,
            fix_flag TEXT,
            full_name TEXT,
            cause TEXT,
            readings TEXT,
            PRIMARY KEY (case_id, hearing_number, record_number));"""
        cursor.execute(sql_command)

        sql_command = """
        CREATE TABLE expert_call_record (
            case_id INTEGER,
            hearing_number INTEGER,
            record_number INTEGER,
            type TEXT,
            fix_flag TEXT,
            full_name TEXT,
            specialty TEXT,
            materials TEXT,
            PRIMARY KEY (case_id, hearing_number, record_number));"""
        cursor.execute(sql_command)

        sql_command = """
        CREATE TABLE petition_record (
            case_id INTEGER,
            hearing_number INTEGER,
            record_number INTEGER,
            type TEXT,
            fix_flag TEXT,
            initiator TEXT,
            subject TEXT,
            decision TEXT,
            PRIMARY KEY (case_id, hearing_number, record_number));"""
        cursor.execute(sql_command)

        sql_command = """
        CREATE TABLE participant (
            participant_id INTEGER PRIMARY KEY,
            role TEXT,
            payload TEXT);"""
        cursor.execute(sql_command)

        connection.commit()
        connection.close()


