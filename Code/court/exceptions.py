class CourtProcessException(Exception):
    def __init__(self, message):
        self.message = message


class DebateOrderBroken(CourtProcessException): pass

class CaseDoubleClose(CourtProcessException): pass

class CaseAlreadyClosed(CourtProcessException): pass

class CaseAlreadyExists(CourtProcessException): pass

class CaseNotExists(CourtProcessException): pass

class HearingAlreadyStarted(CourtProcessException): pass

class HearingAlreadyFinished(CourtProcessException): pass

class HearingNotExists(CourtProcessException): pass

class MaterialDuplication(CourtProcessException): pass

class UseCaseError(CourtProcessException): pass



