from abc import ABC

from court.exceptions import *


class Record(ABC):

    def __init__(self, rcd_type, fix_flag):
        self._rcd_type = rcd_type
        self._fix_flag = fix_flag

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    @property
    def rcd_type(self):
        return self._rcd_type

    @property
    def fix_flag(self):
        return self._fix_flag

    def fix_record(self):
        self._fix_flag = True


class Interrogation(Record):

    def __init__(self, full_name, cause, readings='', fix_flag=False):
        Record.__init__(self, 'Interrogation', fix_flag)
        self._full_name = full_name
        self._cause = cause
        self._readings = readings

    @property
    def readings(self):
        return self._readings

    @property
    def full_name(self):
        return self._full_name

    @property
    def cause(self):
        return self._cause

    def add_question(self, question):
        self._readings += "Question: " + question + "\n"

    def add_answer(self, answer):
        self._readings += "Answer: " + answer + "\n"


class ExpertCall(Record):

    def __init__(self, full_name, specialty, materials=None, fix_flag=False):
        Record.__init__(self, 'Expert call', fix_flag)
        self._full_name = full_name
        self._specialty = specialty
        self._materials = [] if materials is None else materials

    @property
    def materials(self):
        return self._materials

    @property
    def full_name(self):
        return self._full_name

    @property
    def specialty(self):
        return self._specialty

    def add_material(self, material):
        if material not in self._materials:
            self._materials.append(material)
        else:
            raise MaterialDuplication('Material "{0}" is already added to material.'.format(material))


class Petition(Record):

    def __init__(self, initiator, subject, decision=None, fix_flag=False):
        Record.__init__(self, 'Petition', fix_flag)
        self._initiator = initiator
        self._subject = subject
        self._decision = decision

    @property
    def decision(self):
        return self._decision

    @property
    def initiator(self):
        return self._initiator

    @property
    def subject(self):
        return self._subject

    def approve_petition(self):
        self._decision = "Approved"

    def dismiss_petition(self):
        self._decision = "Dismissed"


class Debate(Record):

    def __init__(self, fix_flag=False):
        Record.__init__(self, 'Debate', fix_flag)
        self._speeches = []

    @property
    def speeches(self):
        return self._speeches

    # Debate order:
    # Victim -> Prosecutor -> Lawyer -> Accused
    # Victim may not speak.
    def add_speech(self, speech):
        # If it is first speech it have to be victim's or prosecutor's
        if len(self._speeches) == 0:
            if speech.participant.role == 'Victim' or \
               speech.participant.role == 'Prosecutor':
                self._speeches.append(speech)
            else:
                raise DebateOrderBroken("Only victim or prosecutor can be first.")
        else:
            # It it is not first speech the order have to be respected.
            last_participant = self._speeches[len(self._speeches) - 1].participant.role
            new_participant = speech.participant.role
            if last_participant == 'Victim' and new_participant == 'Prosecutor' or \
               last_participant == 'Prosecutor' and new_participant == 'Lawyer' or \
               last_participant == 'Lawyer' and new_participant == 'Accused':
                self._speeches.append(speech)
            else:
                raise DebateOrderBroken("Debate order is broken.")
