from abc import ABC
import json


class Participant(ABC):

    def __init__(self, role):
        self._role = role

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    @property
    def role(self):
        return self._role


class Secretary(Participant):

    def __init__(self, identifier):
        Participant.__init__(self, 'Secretary')
        self._identifier = identifier

    @property
    def identifier(self):
        return self._identifier

    def to_dct(self):
        dct = {
            'identifier': self._identifier
        }
        return json.dumps(dct)

    @classmethod
    def from_dct(cls, json_str):
        dct = json.loads(json_str)
        secretary = Secretary(
            identifier=dct['identifier']
        )
        return secretary


class Judge(Participant):

    def __init__(self, identifier):
        Participant.__init__(self, 'Judge')
        self._identifier = identifier

    @property
    def identifier(self):
        return self._identifier

    def to_dct(self):
        dct = {
            'identifier': self._identifier
        }
        return json.dumps(dct)

    @classmethod
    def from_dct(cls, json_str):
        dct = json.loads(json_str)
        judge = Judge(
            identifier=dct['identifier']
        )
        return judge


class Prosecutor(Participant):  # Прокурор

    def __init__(self, identity):
        Participant.__init__(self, 'Prosecutor')
        self._identity = identity  # Номер удостоверения

    @property
    def identity(self):
        return self._identity

    def to_dct(self):
        dct = {
            'identity': self._identity
        }
        return json.dumps(dct)

    @classmethod
    def from_dct(cls, json_str):
        dct = json.loads(json_str)
        prosecutor = Prosecutor(
            identity=dct['identity']
        )
        return prosecutor


class Investigator(Participant):  # Следователь

    def __init__(self, rank, passport, identity):
        Participant.__init__(self, 'Investigator')
        self._rank = rank
        self._passport = passport
        self._identity = identity

    @property
    def rank(self):
        return self._rank

    @property
    def passport(self):
        return self._passport

    @property
    def identity(self):
        return self._identity

    def to_dct(self):
        raise NotImplementedError

    @classmethod
    def from_dct(cls, json_str):
        raise NotImplementedError


class Interrogator(Participant):  # Дознаватель

    def __init__(self, rank, passport, identity):
        Participant.__init__(self, 'Interrogator')
        self._rank = rank
        self._passport = passport
        self._identity = identity

    @property
    def rank(self):
        return self._rank

    @property
    def passport(self):
        return self._passport

    @property
    def identity(self):
        return self._identity

    def to_dct(self):
        raise NotImplementedError

    @classmethod
    def from_dct(cls, json_str):
        raise NotImplementedError


class Accused(Participant):

    def __init__(self, passport):
        Participant.__init__(self, 'Accused')
        self._passport = passport

    @property
    def passport(self):
        return self._passport

    def to_dct(self):
        raise NotImplementedError

    @classmethod
    def from_dct(cls, json_str):
        raise NotImplementedError


class Lawyer(Participant):

    def __init__(self, identity):
        Participant.__init__(self, 'Lawyer')
        self._identity = identity

    @property
    def identity(self):
        return self._identity

    def to_dct(self):
        dct = {
            'identity': self._identity
        }
        return json.dumps(dct)

    @classmethod
    def from_dct(cls, json_str):
        dct = json.loads(json_str)
        lawyer = Lawyer(
            identity=dct['identity']
        )
        return lawyer


class Victim(Participant):

    def __init__(self, passport):
        Participant.__init__(self, 'Victim')
        self._passport = passport

    @property
    def passport(self):
        return self._passport

    def to_dct(self):
        raise NotImplementedError

    @classmethod
    def from_dct(cls, json_str):
        raise NotImplementedError

