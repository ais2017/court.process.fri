class Speech:

    def __init__(self, participant, speech_text):
        self._participant = participant
        self._speech_text = speech_text

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    @property
    def participant(self):
        return self._participant

    @property
    def speech_text(self):
        return self._speech_text
