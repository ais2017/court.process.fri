from enum import Enum
from datetime import datetime

from court.domain.protocol import Protocol
from court.exceptions import *


class Hearing:

    def __init__(self, h_type, lawyer, prosecutor,
                 start_time=None, finish_time=None, verdict='Not passed'):
        self._protocol = Protocol()
        self._start_time = start_time
        self._finish_time = finish_time
        self._verdict = verdict
        self._h_type = h_type  # Open or close
        self._lawyer = lawyer
        self._prosecutor = prosecutor

    @property
    def verdict(self):
        return self._verdict

    @property
    def start_time(self):
        return self._start_time

    @property
    def finish_time(self):
        return self._finish_time

    @property
    def h_type(self):
        return self._h_type

    @property
    def lawyer(self):
        return self._lawyer

    @property
    def prosecutor(self):
        return self._prosecutor

    @property
    def protocol(self):
        return self._protocol

    def start_hearing(self):
        if self._start_time is None:
            self._start_time = datetime.now()
        else:
            raise HearingAlreadyStarted('Hearing is already started.')

    def finish_hearing(self, verdict):
        if self._finish_time is None:
            self._finish_time = datetime.now()
            self._verdict = verdict
        else:
            raise HearingAlreadyFinished('Hearing is already finished.')
