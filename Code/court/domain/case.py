from court.exceptions import *


class Case:

    def __init__(self, judge, identifier, verdict=None, state='Open'):
        self._judge = judge
        self._identifier = identifier
        self._verdict = verdict
        self._state = state
        self._hearings = []

    @property
    def judge(self):
        return self._judge

    @property
    def identifier(self):
        return self._identifier

    @property
    def verdict(self):
        return self._verdict

    @property
    def state(self):
        return self._state

    def add_hearing(self, hearing):
        if self._state == 'Close':
            raise CaseAlreadyClosed("You cannot add hearing to closed case")
        self._hearings.append(hearing)

    def get_hearing(self, hearing_number):
        try:
            return self._hearings[hearing_number]
        except IndexError:
            raise HearingNotExists("Hearing with number {0} doesn't exist in case with id {1}"
                                   .format(hearing_number, self._identifier))

    def get_all_hearings(self):
        return self._hearings

    def close(self, verdict):
        if self._state != 'Close':
            self._state = 'Close'
            self._verdict = verdict
        else:
            raise CaseDoubleClose('Case is already closed.')

