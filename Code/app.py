from PyQt4 import QtGui
import sys

import ui.window

from court.usecases.usecases import *
from court.exceptions import *
from court.usecases.gateways import SQLiteCaseGateway


class App(QtGui.QMainWindow, ui.window.Ui_MainWindow):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)
        self.setFixedSize(697, 571)
        self.gateway = SQLiteCaseGateway('/media/data/studies/ais/Code/database/database.db', create_db=True)
        self.case_id = -1
        self.hearing_number = -1
        self.record_number = -1
        self.interrogation_flag = False

        # Set tabs disabled
        self.tabs.setTabEnabled(1, False)
        self.tabs.setTabEnabled(2, False)
        self.tabs.setTabEnabled(3, False)
        self.tabs.setTabEnabled(4, False)
        self.tabs.setTabEnabled(5, False)

        # Welcome
        self.open_case_id.setValidator(QtGui.QIntValidator(0, 999999))
        self.add_hearing_button.clicked.connect(self.add_hearing_wrapper)

        # Start hearing
        self.start_hearing_lawyer_id.setValidator(QtGui.QIntValidator(0, 999))
        self.start_hearing_prosecutor_id.setValidator(QtGui.QIntValidator(0, 999))
        self.start_hearing_button.clicked.connect(self.start_hearing_wrapper)

        # Process
        self.to_call_expert_button.clicked.connect(self.to_call_expert_wrapper)
        self.to_petition_button.clicked.connect(self.to_petition_wrapper)
        self.to_interrogation_button.clicked.connect(self.to_interrogation_wrapper)
        self.close_case_button.clicked.connect(self.close_case_wrapper)

        # Expert cal
        self.expert_call_button.clicked.connect(self.expert_call_wrapper)
        self.expert_call_back_button.clicked.connect(self.to_process_wrapper)

        # Petition
        self.petition_button.clicked.connect(self.petiton_wrapper)
        self.petition_back_button.clicked.connect(self.to_process_wrapper)

        # Interrogation
        self.interrogation_start.clicked.connect(self.interrogation_start_wrapper)
        self.interrogation_complete.clicked.connect(self.interrogation_complete_wrapper)
        self.interrogation_add_question.clicked.connect(self.interrogation_add_question_wrapper)
        self.interrogation_add_answer.clicked.connect(self.interrogation_add_answer_wrapper)
        self.interrogation_back.clicked.connect(self.to_process_wrapper)

        # Add cases
        cases = []
        for i in range(self.get_cases_table.rowCount()):
            case = (int(self.get_cases_table.item(i, 0).text()), int(self.get_cases_table.item(i, 1).text()))
            cases.append(case)
        for case in cases:
            open_case(self.gateway, case[1], case[0])

    def add_hearing_wrapper(self):
        self.add_hearing_status.setText('')
        if self.open_case_id.text() == '':
            self.add_hearing_status.setText('Вы не ввели ID дела')
            return
        cases = []
        for i in range(self.get_cases_table.rowCount()):
            cases.append(int(self.get_cases_table.item(i, 0).text()))
        self.case_id = int(self.open_case_id.text())
        if self.case_id not in cases:
            self.add_hearing_status.setText('Дела с таким ID нет')
            return
        self.statusBar.showMessage("ID текущего дела: " + str(self.case_id))
        self.tabs.setTabEnabled(1, True)
        self.tabs.setCurrentIndex(1)

    def to_process_wrapper(self):
        self.tabs.setTabEnabled(2, True)
        self.tabs.setCurrentIndex(2)
        self.tabs.setTabEnabled(0, False)
        self.tabs.setTabEnabled(1, False)
        self.tabs.setTabEnabled(3, False)
        self.tabs.setTabEnabled(4, False)
        self.tabs.setTabEnabled(5, False)

    def to_call_expert_wrapper(self):
        self.tabs.setTabEnabled(3, True)
        self.tabs.setCurrentIndex(3)
        self.tabs.setTabEnabled(0, False)
        self.tabs.setTabEnabled(1, False)
        self.tabs.setTabEnabled(2, False)
        self.tabs.setTabEnabled(4, False)
        self.tabs.setTabEnabled(5, False)

    def to_petition_wrapper(self):
        self.tabs.setTabEnabled(4, True)
        self.tabs.setCurrentIndex(4)
        self.tabs.setTabEnabled(0, False)
        self.tabs.setTabEnabled(1, False)
        self.tabs.setTabEnabled(2, False)
        self.tabs.setTabEnabled(3, False)
        self.tabs.setTabEnabled(5, False)

    def to_interrogation_wrapper(self):
        self.tabs.setTabEnabled(5, True)
        self.tabs.setCurrentIndex(5)
        self.tabs.setTabEnabled(0, False)
        self.tabs.setTabEnabled(1, False)
        self.tabs.setTabEnabled(2, False)
        self.tabs.setTabEnabled(3, False)
        self.tabs.setTabEnabled(4, False)

    def close_case_wrapper(self):
        if self.close_verdict.text() == '':
            self.close_case_status.setText('Заполнены не все поля')
            return
        case_id = self.case_id
        verdict = self.close_verdict.text()
        try:
            finish_hearing(self.gateway, case_id, self.hearing_number, verdict)
            close_case(self.gateway, case_id, verdict)
            self.close_case_status.setText('Дело успешно закрыто')
            self.tabs.setTabEnabled(0, True)
            self.tabs.setCurrentIndex(0)
            self.tabs.setTabEnabled(1, False)
            self.tabs.setTabEnabled(2, False)
            self.tabs.setTabEnabled(3, False)
            self.tabs.setTabEnabled(4, False)
            self.tabs.setTabEnabled(5, False)
        except CaseNotExists:
            self.close_case_status.setText('Дело c таким ID не существует')
        except CaseDoubleClose:
            self.close_case_status.setText('Дело уже закрыто')

    def start_hearing_wrapper(self):
        if self.start_hearing_lawyer_id.text() == '' or self.start_hearing_prosecutor_id.text() == '':
            self.start_hearing_status.setText('Заполнены не все поля')
            return
        case_id = self.case_id
        lawyer_id = int(self.start_hearing_lawyer_id.text())
        prosecutor_id = int(self.start_hearing_prosecutor_id.text())
        hearing_type = 'Open' if self.open_hearing.isChecked() else 'Close'
        try:
            add_hearing_to_case(self.gateway, case_id, hearing_type, lawyer_id, prosecutor_id)
            hearings = get_hearings_by_case_id(self.gateway, self.case_id)
            self.hearing_number = len(hearings) - 1
            start_hearing(self.gateway, case_id, self.hearing_number)
            self.statusBar.clearMessage()
            self.statusBar.showMessage("ID текущего дела: " + str(self.case_id) + " :: Слушание #" + str(self.hearing_number + 1))
            self.tabs.setCurrentIndex(2)
            self.tabs.setTabEnabled(0, False)
            self.tabs.setTabEnabled(1, False)
            self.tabs.setTabEnabled(2, True)
            self.tabs.setTabEnabled(3, False)
            self.tabs.setTabEnabled(4, False)
            self.tabs.setTabEnabled(5, False)
            self.status.setText("ID текущего дела: " + str(self.case_id) + " :: Слушание #" + str(self.hearing_number + 1))
        except CaseAlreadyClosed:
            self.start_hearing_status.setText('Дело уже закрыто')
        except CaseNotExists:
            self.start_hearing_status.setText('Дело c таким ID не существует')

    def expert_call_wrapper(self):
        if self.expert_call_full_name.text() == '' or self.expert_call_specialty.text() == '':
            self.expert_call_status.setText('Заполнены не все поля')
            return
        case_id = self.case_id
        hearing_number = self.hearing_number
        full_name = self.expert_call_full_name.text()
        specialty = self.expert_call_specialty.text()
        materials = self.expert_call_materials.toPlainText().split('\n')
        try:
            call_expert(self.gateway, case_id, hearing_number, full_name, specialty, materials)
            self.tabs.setCurrentIndex(2)
            self.tabs.setTabEnabled(0, False)
            self.tabs.setTabEnabled(1, False)
            self.tabs.setTabEnabled(2, True)
            self.tabs.setTabEnabled(3, False)
            self.tabs.setTabEnabled(4, False)
            self.tabs.setTabEnabled(5, False)
            self.statusBar.clearMessage()
            self.statusBar.showMessage("Запись о вызове эксперта добавлена успешно")
            # self.expert_call_status.setText('Запись о вызове эксперта добавлена успешно')
        except CaseNotExists:
            self.expert_call_status.setText('Дело c таким ID не существует')
        except HearingNotExists:
            self.expert_call_status.setText('Слушание с таким номером не существует')
        except MaterialDuplication:
            self.expert_call_status.setText('Обнаружено дублирование материалов')
        finally:
            self.expert_call_materials.clear()

    def petiton_wrapper(self):
        if self.petition_initiator.text() == '' or self.petition_subject.text() == '':
            self.petition_status.setText('Заполнены не все поля')
            return
        case_id = self.case_id
        hearing_number = self.hearing_number
        initiator = self.petition_initiator.text()
        subject = self.petition_subject.text()
        try:
            submit_petition(self.gateway, case_id, hearing_number, initiator, subject)
            self.tabs.setCurrentIndex(2)
            self.tabs.setTabEnabled(0, False)
            self.tabs.setTabEnabled(1, False)
            self.tabs.setTabEnabled(2, True)
            self.tabs.setTabEnabled(3, False)
            self.tabs.setTabEnabled(4, False)
            self.tabs.setTabEnabled(5, False)
            self.statusBar.clearMessage()
            self.statusBar.showMessage("Запись о подаче ходатайства успешно добавлена")
            # self.petition_status.setText('Запись о подаче ходатайства успешно добавлена')
        except CaseNotExists:
            self.petition_status.setText('Дело c таким ID не существует')
        except HearingNotExists:
            self.petition_status.setText('Слушание с таким номером не существует')

    def interrogation_start_wrapper(self):
        if self.interrogation_full_name.text() == '' or self.interrogation_cause.text() == '':
            self.interrogation_status.setText('Заполнены не все поля')
            return
        case_id = self.case_id
        hearing_number = self.hearing_number
        full_name = self.interrogation_full_name.text()
        cause = self.interrogation_cause.text()
        try:
            self.record_number = interrogate_witness(self.gateway, case_id, hearing_number, full_name, cause)
            self.interrogation_status.setText('Допрос успешно начат')
            print(self.record_number)
            self.interrogation_back.setEnabled(False)
            self.interrogation_start.setEnabled(False)
            self.interrogation_flag = True
        except CaseNotExists:
            self.expert_call_status.setText('Дело c таким ID не существует')
        except HearingNotExists:
            self.expert_call_status.setText('Слушание с таким номером не существует')

    def interrogation_add_question_wrapper(self):
        if self.interrogation_question.text() == '':
            self.interrogation_status.setText('Вы не ввели вопрос')
            return
        if self.interrogation_flag is False:
            self.interrogation_status.setText('Вы ещё не начали допрос')
            return
        case_id = self.case_id
        hearing_number = self.hearing_number
        record_number = self.record_number
        print(record_number)
        question = self.interrogation_question.text()
        add_question(self.gateway, case_id, hearing_number, record_number, question)
        self.interrogation_status.setText('Вопрос успешно добавлен')

    def interrogation_add_answer_wrapper(self):
        if self.interrogation_answer.text() == '':
            self.interrogation_status.setText('Вы не ввели ответ')
            return
        if self.interrogation_flag is False:
            self.interrogation_status.setText('Вы ещё не начали допрос')
            return
        case_id = self.case_id
        hearing_number = self.hearing_number
        record_number = self.record_number
        print(record_number)
        answer = self.interrogation_question.text()
        add_answer(self.gateway, case_id, hearing_number, record_number, answer)
        self.interrogation_status.setText('Ответ успешно добавлен')

    def interrogation_complete_wrapper(self):
        self.statusBar.clearMessage()
        self.statusBar.showMessage("Запись о допросе успешно добавлена")
        self.interrogation_flag = False
        self.tabs.setTabEnabled(2, True)
        self.tabs.setCurrentIndex(2)
        self.tabs.setTabEnabled(0, False)
        self.tabs.setTabEnabled(1, False)
        self.tabs.setTabEnabled(3, False)
        self.tabs.setTabEnabled(4, False)
        self.tabs.setTabEnabled(5, False)


def main():
    app = QtGui.QApplication(sys.argv)
    form = App()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
